import * as fs from 'fs';


class Point {
    constructor(
        public x: number,
        public y: number
    ){}
    public toString(): string {
        return `x: ${this.x}, y: ${this.y}`;
    }
    public equals(other: Point): boolean {
        return ((this.x == other.x) && (this.y == other.y));
    }
}

const solve = (m: number, n: number) => {
    let p_position = new Point(0, 0);
    let position = new Point(0, 0);
    while (true) {
        if (position.x == p_position.x && position.y == p_position.y) {
            p_position.x = position.x;
            p_position.y = position.y;
            position.x ++;
            position.y ++;
        } else if (position.equals(new Point(0, 0))) {
            return "bottom left";
        } else if (position.equals(new Point(0, n))) {
            return "bottom right";
        } else if (position.equals(new Point(m, n))) {
            return "top right";
        } else if (position.equals(new Point(m, 0))) {
            return "top left";
        } else if ((position.x - p_position.x) == 1 && (position.y - p_position.y) == 1) {
            if (position.x < m && position.y < n) {
                p_position.x = position.x;
                p_position.y = position.y;
                position.x += 1;
                position.y += 1;
            }
            else if (position.x == m) {
                p_position.x = position.x;
                p_position.y = position.y;
                position.x -= 1;
                position.y += 1;
            }
            else if (position.y == n) {
                p_position.x = position.x;
                p_position.y = position.y;
                position.x += 1;
                position.y -= 1;
            }
        }
        else if ((p_position.x - position.x) == 1 && (p_position.y - position.y) == 1) {
            if (position.x > 0 && position.y > 0) {
                p_position.x = position.x;
                p_position.y = position.y;
                position.x -= 1;
                position.y -= 1;
            }
            else if (position.x == 0) {
                p_position.x = position.x;
                p_position.y = position.y;
                position.x += 1;
                position.y -= 1;
            }
            else if (position.y == 0) {
                p_position.x = position.x;
                p_position.y = position.y;
                position.x -= 1;
                position.y += 1;
            }
        }
        else if ((p_position.x - position.x) == 1 && (position.y - p_position.y) == 1) {
            if (position.x > 0 && position.y < n) {
                p_position.x = position.x;
                p_position.y = position.y;
                position.x -= 1;
                position.y += 1;
            }
            else if (position.x == 0) {
                p_position.x = position.x;
                p_position.y = position.y;
                position.x += 1;
                position.y += 1;
            }
            else if (position.y == n) {
                p_position.x = position.x;
                p_position.y = position.y;
                position.x -= 1;
                position.y -= 1;
            }
        }
        else if ((position.x - p_position.x) == 1 && (p_position.y - position.y) == 1) {
            if (position.x < m && position.y > 0) {
                p_position.x = position.x;
                p_position.y = position.y;
                position.x += 1;
                position.y -= 1;
            }
            else if (position.x == m) {
                p_position.x = position.x;
                p_position.y = position.y;
                position.x -= 1;
                position.y -= 1;
            }
            else if (position.y == 0) {
                p_position.x = position.x;
                p_position.y = position.y;
                position.x += 1;
                position.y += 1;
            }
        }
        
    }
}

const findPattern = (until: number) => {
    const file = fs.createWriteStream('save.txt');
    for (let i = 1; i < until+1; i++) {
        for (let j = 1; j < until+1; j++) {
            const res = solve(i, j);
            file.write(`m: ${i}, n: ${j}, ending hole: ${res}\n`);
        }
    }
    file.close();
}

findPattern(100);


