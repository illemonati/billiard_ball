from numba import jit, jitclass, njit
from numba import int32

spec = [
    ('x', int32),
    ('y', int32)
]

# @jitclass(spec)
class Point():
    def __init__(self, x, y):
        self.x = x
        self.y = y
    def __str__(self):
        return f"x: {self.x}, y: {self.y}"
    def __eq__(self, other):
        if not isinstance(other, Point):
            return NotImplemented
        return self.x == other.x and self.y == other.y            


@jit()
def solve(m, n):
    p_position = Point(0, 0)
    position = Point(1, 1)
    holes = [Point(0, 0), Point(m, n), Point(m, 0), Point(0, n)]
    result = None
    while True:
        # print(f"pposition : {p_position}, postion: {position}")
        if position.x == p_position.x and position.y == p_position.y:
            p_position.x = position.x
            p_position.y = position.y
            position.x += 1
            position.y += 1
        elif position in holes:
            result = position
            break
        elif (position.x - p_position.x) == 1 and (position.y - p_position.y) == 1:
            if position.x < m and position.y < n:
                p_position.x = position.x
                p_position.y = position.y
                position.x += 1
                position.y += 1
            elif position.x == m:
                p_position.x = position.x
                p_position.y = position.y
                position.x -= 1
                position.y += 1
            elif position.y == n:
                p_position.x = position.x
                p_position.y = position.y
                position.x += 1
                position.y -= 1
        elif (p_position.x - position.x) == 1 and (p_position.y - position.y) == 1:
            if position.x > 0 and position.y > 0:
                p_position.x = position.x
                p_position.y = position.y
                position.x -= 1
                position.y -= 1
            elif position.x == 0:
                p_position.x = position.x
                p_position.y = position.y
                position.x += 1
                position.y -= 1
            elif position.y == 0:
                p_position.x = position.x
                p_position.y = position.y
                position.x -= 1
                position.y += 1
        elif (p_position.x - position.x) == 1 and (position.y - p_position.y) == 1:
            if position.x > 0 and position.y < n:
                p_position.x = position.x
                p_position.y = position.y
                position.x -= 1
                position.y += 1
            elif position.x == 0:
                p_position.x = position.x
                p_position.y = position.y
                position.x += 1
                position.y += 1
            elif position.y == n:
                p_position.x = position.x
                p_position.y = position.y
                position.x -= 1
                position.y -= 1
        elif (position.x - p_position.x) == 1 and (p_position.y - position.y) == 1:
            if position.x < m and position.y > 0:
                p_position.x = position.x
                p_position.y = position.y
                position.x += 1
                position.y -= 1
            elif position.x == m:
                p_position.x = position.x
                p_position.y = position.y
                position.x -= 1
                position.y -= 1
            elif position.y == 0:
                p_position.x = position.x
                p_position.y = position.y
                position.x += 1
                position.y += 1
    if result == Point(0, 0):
        return 'bottem left'
    elif result == Point(0, n):
        return 'bottem right'
    elif result == Point(m, n):
        return 'top right'
    elif result == Point(m, 0):
        return 'top left'

@jit()
def find_pattern(until):
    file = open("save.txt", "w+")
    for i in range(1, until+1):
        for j in range(1, until+1):
            m = i
            n = j
            s = solve(m, n)
            file.write("m: {}, n: {}, ending hole: {}\n".format(m, n, s))
    file.close()


if __name__ == "__main__":
    find_pattern(100)
