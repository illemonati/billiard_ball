use std::fs::File;
use std::io::Write;


#[derive(PartialEq, PartialOrd)]
struct Point {
    x: i32,
    y: i32
}


impl Point {
    fn new(x: i32, y: i32) -> Point {
        Point {
            x, y
        }
    }
    fn to_string(&self) -> String {
        format!("x: {}, y: {}", self.x, self.y)
    }
}

fn solve(m: i32, n: i32) -> &'static str {
    let mut p_position = Point::new(0, 0);
    let mut position = Point::new(1, 1);
    loop {
        if (position.x == p_position.x && position.y == p_position.y) {
            p_position.x = position.x;
            p_position.y = position.y;
            position.x += 1;
            position.y += 1;
        } else if (position == Point::new(0, 0)) {
            return "bottom left";
        } else if (position == Point::new(0, n)) {
            return "bottom right";
        } else if (position == Point::new(m, n)) {
            return "top right";
        } else if (position == Point::new(m, 0)) {
            return "top left";
        } else if ((position.x - p_position.x) == 1 && (position.y - p_position.y) == 1) {
            if (position.x < m && position.y < n) {
                p_position.x = position.x;
                p_position.y = position.y;
                position.x += 1;
                position.y += 1;
            }
            else if (position.x == m) {
                p_position.x = position.x;
                p_position.y = position.y;
                position.x -= 1;
                position.y += 1;
            }
            else if (position.y == n) {
                p_position.x = position.x;
                p_position.y = position.y;
                position.x += 1;
                position.y -= 1;
            }
        }
        else if ((p_position.x - position.x) == 1 && (p_position.y - position.y) == 1) {
            if (position.x > 0 && position.y > 0) {
                p_position.x = position.x;
                p_position.y = position.y;
                position.x -= 1;
                position.y -= 1;
            }
            else if (position.x == 0) {
                p_position.x = position.x;
                p_position.y = position.y;
                position.x += 1;
                position.y -= 1;
            }
            else if (position.y == 0) {
                p_position.x = position.x;
                p_position.y = position.y;
                position.x -= 1;
                position.y += 1;
            }
        }
        else if ((p_position.x - position.x) == 1 && (position.y - p_position.y) == 1) {
            if (position.x > 0 && position.y < n) {
                p_position.x = position.x;
                p_position.y = position.y;
                position.x -= 1;
                position.y += 1;
            }
            else if (position.x == 0) {
                p_position.x = position.x;
                p_position.y = position.y;
                position.x += 1;
                position.y += 1;
            }
            else if (position.y == n) {
                p_position.x = position.x;
                p_position.y = position.y;
                position.x -= 1;
                position.y -= 1;
            }
        }
        else if ((position.x - p_position.x) == 1 && (p_position.y - position.y) == 1) {
            if (position.x < m && position.y > 0) {
                p_position.x = position.x;
                p_position.y = position.y;
                position.x += 1;
                position.y -= 1;
            }
            else if (position.x == m) {
                p_position.x = position.x;
                p_position.y = position.y;
                position.x -= 1;
                position.y -= 1;
            }
            else if (position.y == 0) {
                p_position.x = position.x;
                p_position.y = position.y;
                position.x += 1;
                position.y += 1;
            }
        }
        
    }
}

fn find_pattern(until: i32) {
    let mut file = File::create("save.txt").unwrap();
    for i in 1..until+1 {
        for j in 1..until+1 {
            let res = solve(i, j);
            write!(file, "m: {}, n: {}, ending hole: {}\n", i, j, res);
        }
    } 
}


fn main() {
    find_pattern(100);
}
