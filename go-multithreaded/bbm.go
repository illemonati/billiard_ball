package main

import (
	"fmt"
	"log"
	"os"
)

// Point : A point
type Point struct {
	x, y int
}

func (p Point) String() string {
	return fmt.Sprintf("x: %d, y: %d", p.x, p.y)
}

func solve(m, n int) string {
	pPosition := Point{0, 0}
	position := Point{1, 1}
	for {
		if position.x == pPosition.x && position.y == pPosition.y {
			pPosition.x = position.x
			pPosition.y = position.y
			position.x++
			position.y++
		} else if (position == Point{0, 0}) {
			return "bottom left"
		} else if (position == Point{0, n}) {
			return "bottom right"
		} else if (position == Point{m, n}) {
			return "top right"
		} else if (position == Point{m, 0}) {
			return "top left"
		} else if (position.x-pPosition.x) == 1 && (position.y-pPosition.y) == 1 {
			if position.x < m && position.y < n {
				pPosition.x = position.x
				pPosition.y = position.y
				position.x++
				position.y++
			} else if position.x == m {
				pPosition.x = position.x
				pPosition.y = position.y
				position.x--
				position.y++
			} else if position.y == n {
				pPosition.x = position.x
				pPosition.y = position.y
				position.x++
				position.y--
			}
		} else if (pPosition.x-position.x) == 1 && (pPosition.y-position.y) == 1 {
			if position.x > 0 && position.y > 0 {
				pPosition.x = position.x
				pPosition.y = position.y
				position.x--
				position.y--
			} else if position.x == 0 {
				pPosition.x = position.x
				pPosition.y = position.y
				position.x++
				position.y--
			} else if position.y == 0 {
				pPosition.x = position.x
				pPosition.y = position.y
				position.x--
				position.y++
			}
		} else if (pPosition.x-position.x) == 1 && (position.y-pPosition.y) == 1 {
			if position.x > 0 && position.y < n {
				pPosition.x = position.x
				pPosition.y = position.y
				position.x--
				position.y++
			} else if position.x == 0 {
				pPosition.x = position.x
				pPosition.y = position.y
				position.x++
				position.y++
			} else if position.y == n {
				pPosition.x = position.x
				pPosition.y = position.y
				position.x--
				position.y--
			}
		} else if (position.x-pPosition.x) == 1 && (pPosition.y-position.y) == 1 {
			if position.x < m && position.y > 0 {
				pPosition.x = position.x
				pPosition.y = position.y
				position.x++
				position.y--
			} else if position.x == m {
				pPosition.x = position.x
				pPosition.y = position.y
				position.x--
				position.y--
			} else if position.y == 0 {
				pPosition.x = position.x
				pPosition.y = position.y
				position.x++
				position.y++
			}
		}

	}
}

func findRange(group, until int, c chan []string) {
	results := make([]string, until)
	for j := 1; j < until+1; j++ {
		res := solve(group, j)
		results[j-1] = fmt.Sprintf("m: %d, n: %d, ending hole: %s\n", group, j, res)
	}
	c <- results
}

func findPattern(until int) {
	file, err := os.Create("save.txt")
	defer file.Close()
	if err != nil {
		log.Fatal(err)
	}
	c := make(chan []string, until)
	for i := 1; i < until+1; i++ {
		findRange(i, until, c)
	}
	for u := 0; u < until; u++ {
		results := <-c
		for _, item := range results {
			file.WriteString(item)
		}
	}
}

func main() {
	findPattern(100)
}
