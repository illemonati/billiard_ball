# include <iostream>
# include <fstream>
# include <string>
# include <sstream>

using namespace std;

class Point {
public:
    int x;
    int y;
    Point(int x, int y) : x(x), y(y) {}
    string str() {
        stringstream ss;
        ss << "x: " << x << ", y: " << y;
        return ss.str();
    }
    bool operator==(const Point &p2) {
        return (x == p2.x) && (y == p2.y);
    }
};


string solve(int m, int n) {
    Point p_position = Point(0, 0);
    Point position = Point(1, 1);
    while (true) {
        if (position.x == p_position.x and position.y == p_position.y) {
            p_position.x = position.x;
            p_position.y = position.y;
            position.x ++;
            position.y ++;
        } else if (position == Point(0, 0)) {
            return "bottom left";
        } else if (position == Point(0, n)) {
            return "bottom right";
        } else if (position == Point(m, n)) {
            return "top right";
        } else if (position == Point(m, 0)) {
            return "top left";
        } else if ((position.x - p_position.x) == 1 and (position.y - p_position.y) == 1) {
            if (position.x < m && position.y < n) {
                p_position.x = position.x;
                p_position.y = position.y;
                position.x += 1;
                position.y += 1;
            }
            else if (position.x == m) {
                p_position.x = position.x;
                p_position.y = position.y;
                position.x -= 1;
                position.y += 1;
            }
            else if (position.y == n) {
                p_position.x = position.x;
                p_position.y = position.y;
                position.x += 1;
                position.y -= 1;
            }
        }
        else if ((p_position.x - position.x) == 1 and (p_position.y - position.y) == 1) {
            if (position.x > 0 && position.y > 0) {
                p_position.x = position.x;
                p_position.y = position.y;
                position.x -= 1;
                position.y -= 1;
            }
            else if (position.x == 0) {
                p_position.x = position.x;
                p_position.y = position.y;
                position.x += 1;
                position.y -= 1;
            }
            else if (position.y == 0) {
                p_position.x = position.x;
                p_position.y = position.y;
                position.x -= 1;
                position.y += 1;
            }
        }
        else if ((p_position.x - position.x) == 1 and (position.y - p_position.y) == 1) {
            if (position.x > 0 and position.y < n) {
                p_position.x = position.x;
                p_position.y = position.y;
                position.x -= 1;
                position.y += 1;
            }
            else if (position.x == 0) {
                p_position.x = position.x;
                p_position.y = position.y;
                position.x += 1;
                position.y += 1;
            }
            else if (position.y == n) {
                p_position.x = position.x;
                p_position.y = position.y;
                position.x -= 1;
                position.y -= 1;
            }
        }
        else if ((position.x - p_position.x) == 1 and (p_position.y - position.y) == 1) {
            if (position.x < m and position.y > 0) {
                p_position.x = position.x;
                p_position.y = position.y;
                position.x += 1;
                position.y -= 1;
            }
            else if (position.x == m) {
                p_position.x = position.x;
                p_position.y = position.y;
                position.x -= 1;
                position.y -= 1;
            }
            else if (position.y == 0) {
                p_position.x = position.x;
                p_position.y = position.y;
                position.x += 1;
                position.y += 1;
            }
        }
        
    }
}

void findPattern(int until) {
    auto file = ofstream("save.txt");
    for (int i = 1; i <= until; ++i) {
        for (int j = 1; j <= until; ++j) {
            auto res = solve(i, j);
            stringstream ss;
            ss << "m: " << i << ", " << "n: " << j << ", " << "ending hole: " << res;
            file << ss.str() << endl;
        }
    }
    file.close();
}

int main() {
    findPattern(100);
}




